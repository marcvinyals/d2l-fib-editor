# D2L FIB Editor

A robot that creates fill-in the blanks type questions following the
same template in D2L.

## Dependencies

* Python
* [Selenium python bindings](https://pypi.org/project/selenium/)
* [Chrome Selenium driver](https://chromedriver.chromium.org/downloads) (Firefox does not expose shadow DOM as of 2022)

## Usage

1. Create a template question in D2L.
2. Generate one CSV file containing all questions, matching the template. Every question should begin with ``NewQuestion,FIB`` line, followed by a ``Title`` line and as many ``Text`` and ``Blank`` lines as the template. See [blanks.csv](blanks.csv) for an example.
3. Edit `config.json` appropriately. Alternatively, configuration options can be passed as command line parameters, which override any parameter possibly set before.
3. Run ``./blanks.py < questions.csv``.

## Writing CSV question files

The format is a subset of the standard D2L CSV format. Every question should start with a line
```
NewQuestion,FIB
```
followed by a line
```
Title,"<title (string)>"
```
The next lines should be either of the form
```
Text,"<question text (string)>"
```
or
```
Blank,"<answer (string)>",<weight (integer)>
```
in the same order as they appear in the template. See [blanks.csv](blanks.csv) for an example. Any other lines are ignored and treated as comments.
