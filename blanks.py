#!/usr/bin/env python3

""" D2L Fill-in the blanks editor

A robot that creates fill-in the blanks type questions following the
same template in D2L.

(c) 2022 Marc Vinyals

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

from seleniium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import csv
import sys
import argparse
import json

parser = argparse.ArgumentParser()
parser.add_argument('--config', type=argparse.FileType('r'))
parser.add_argument('--username')
parser.add_argument('--password')
parser.add_argument('--course-id', type=int)
parser.add_argument('--section-name')
parser.add_argument('--template-name')
parser.add_argument('--base-url')

def fill_config(config):
    global username
    if 'username' in config:
        username = config['username']
    global password
    if 'password' in config:
        password = config['password']
    global baseurl
    if 'baseurl' in config:
        baseurl = config['baseurl']
    global courseid
    if 'course_id' in config:
        courseid = config['course_id']
    global section_name
    if 'section_name' in config:
        section_name = config['section_name']
    global template_name
    if 'template_name' in config:
        template_name = config['template_name']

args = parser.parse_args()
try:
    if args.config:
        config = json.load(args.config)
        fill_config(config)
    else:
        with open('config.json','r') as f:
            config = json.load(f)
            fill_config(config)
except Exception:
    pass

fill_config({k:v for k,v in vars(args).items() if v is not None})
test = 'devcop' in baseurl

driver = webdriver.Chrome()
driver.implicitly_wait(5)
driver.maximize_window()

def get_tag_by_text(tag, text):
    elements = driver.find_elements(By.TAG_NAME, tag)
    for element in elements:
        if text == element.text:
            return element
    return None

def get_button_by_text(text):
    return get_tag_by_text('button', text)

def get_menu_item_by_text(text):
    return get_tag_by_text('d2l-menu-item', text)

def mun_login():
    driver.get(baseurl)
    login = driver.find_element(By.ID, "mun-login")
    login.click()
    username_ = driver.find_element(By.NAME, "username")
    password_ = driver.find_element(By.NAME, "password")
    username_.send_keys(username)
    password_.send_keys(password)
    password_.submit()

def test_login():
    driver.get(baseurl)
    username_ = driver.find_element(By.NAME, "userName")
    password_ = driver.find_element(By.NAME, "password")
    username_.send_keys(username)
    password_.send_keys(password)
    xlogin = get_button_by_text("Log In")
    xlogin.click()

def open_library():
    driver.get(f"{baseurl}/d2l/lms/quizzing/admin/quizzes_manage.d2l?ou={courseid}")
    driver.find_element(By.LINK_TEXT, "Question Library").click()

def new_fib():
    driver.switch_to.frame("content")
    driver.switch_to.frame("listFrame")
    driver.find_element(By.CSS_SELECTOR, "#z_q .d2l-dropdown-opener").click()
    get_menu_item_by_text("Fill in the Blanks Question (FIB)").click()

def open_template():
    driver.switch_to.frame("content")
    driver.switch_to.frame("listFrame")
    driver.find_element(By.LINK_TEXT, section_name).click()
    driver.find_element(By.LINK_TEXT, template_name).click()

def move_to_new_window():
    if test:
        roothandle = driver.current_window_handle
        childhandle = None
        for handle in driver.window_handles:
            if handle != roothandle:
                childhandle = handle
                break
        driver.switch_to.window(childhandle)
        driver.switch_to.frame("Body")

def make_copy():
    if test:
        driver.switch_to.parent_frame()
        driver.switch_to.frame("Footer")
    xsave = get_button_by_text("Save and Copy")
    xsave.click()
    if test:
        driver.switch_to.parent_frame()
        driver.switch_to.frame("Body")

def edit_title(title, points=None):
    qlabel = get_tag_by_text('label', 'Title')
    qtitle = driver.find_element(By.ID, qlabel.get_attribute("for"))
    qtitle.clear()
    qtitle.send_keys(title)
    if points is None:
        return
    qtitle.send_keys(Keys.TAB)
    qpoints = driver.switch_to.active_element
    qpoints.send_keys(Keys.DELETE)
    qpoints.send_keys(str(points))

def edit_question_text(i, text):
    qlabel = driver.find_element(By.ID, f"labelTextId{i}")
    qeditor = driver.find_element(By.ID, qlabel.get_attribute("for"))
    qframe = qeditor.shadow_root.find_element(By.CSS_SELECTOR, "iframe")
    driver.switch_to.frame(qframe)
    qtext = driver.find_element(By.TAG_NAME, "body")
    qtext.clear()
    qtext.send_keys(text)
    driver.switch_to.parent_frame()

def edit_question_blank(i, text, weight):
    qblank = driver.find_element(By.XPATH, f"//label[@title='Blank {i} size']/..//d2l-table-wrapper//input[1]")
    qblank.clear()
    qblank.send_keys(text)
    qblank.send_keys(Keys.TAB)

    qweight = driver.switch_to.active_element
    qweight.send_keys(Keys.DELETE)
    qweight.send_keys(str(weight))

def save():
    if test:
        driver.switch_to.parent_frame()
        driver.switch_to.frame("Footer")
    xsave = get_button_by_text("Save")
    xsave.click()

def save_and_copy():
    if test:
        driver.switch_to.parent_frame()
        driver.switch_to.frame("Footer")
    xsave = get_button_by_text("Save and Copy")
    xsave.click()
    if test:
        driver.switch_to.parent_frame()
        driver.switch_to.frame("Body")

def navigate():
    if test:
        test_login()
    else:
        mun_login()
    open_library()
    open_template()
    move_to_new_window()
    make_copy()

def add_question(question):
    ntexts=0
    nblanks=0
    for line in question:
        if line[0]=="Title":
            print(f"Question title: {line[1]}")
            edit_title(line[1])
        elif line[0]=="Text":
            ntexts+=1
            edit_question_text(ntexts,line[1])
        elif line[0]=="Blank":
            nblanks+=1
            edit_question_blank(nblanks,line[1],line[2])
        else:
            raise ValueError(line[0])
    save_and_copy()

# We parse questions first to ensure no syntax errors

questionlist = []
question = None
def append_question():
    global question
    global questionlist
    if question is not None:
        questionlist.append(question)
    question = []

for row in csv.reader(sys.stdin):
    if len(row)==0: continue
    if row[0]=="NewQuestion":
        if row[1]=="FIB":
            append_question()
        else:
            raise ValueError(row[1])
    elif row[0] in {"Text", "Blank", "Title"}:
        question.append(row)
append_question()

print(f"Successfully parsed {len(questionlist)} questions")



# Now we add questions

navigate()

for nq,question in enumerate(questionlist):
    print(f"Attempting to add question {nq+1}")
    add_question(question)
    print(f"Successfully added question {nq+1}")

print(f"Successfully added {len(questionlist)} questions. Exiting.")
